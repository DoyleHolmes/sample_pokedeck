part of 'shared.dart';

const double defaultMargin = 24;

Color mainColor = Colors.grey;
Color accentColor1 = const Color(0xFF5DB960);
Color accentColor2 = const Color(0xFF4BB14E);
Color accentColor3 = const Color(0xFFADADAD);