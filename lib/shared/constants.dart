part of 'shared.dart';

const String baseUrl =
    'https://pokeapi.co/api/v2/';
const String getDataPokemon =
    'pokemon/';
const String getDataPokemon100 =
    'pokemon?limit=100&offset=200';
const String urlImgPokemon =
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

const defaultPadding = 24;
const defaultDoublePadding = 24.0;
const defaultAvatarRadius = 45.0;

const String columnIdStr = 'idStr';
const String columnName = 'name';

List<Pokemon> dummylist = [
  Pokemon(name: 'name', url: ''),
  Pokemon(name: 'name', url: ''),
];