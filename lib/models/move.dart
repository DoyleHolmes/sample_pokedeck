part of 'models.dart';

class Move {
  final String name;
  final String url;

  Move({required this.name, required this.url});

  factory Move.fromJson(Map<String, dynamic> json) => _moveFromJson(json);
}

Move _moveFromJson(Map<String, dynamic> json) {

  return Move(
    name: json['name'] ?? "",
    url: json['url'] ?? "",
  );
}
