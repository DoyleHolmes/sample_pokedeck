part of 'models.dart';

class Typeslot {
  final int slot;
  final Typee type;

  Typeslot({required this.slot, required this.type});

  factory Typeslot.fromJson(Map<String, dynamic> json) => _typeslotFromJson(json);
}

Typeslot _typeslotFromJson(Map<String, dynamic> json) {
  var dataJson = json['type'];
  Typee data = Typee.fromJson(dataJson);

  return Typeslot(
    slot: json['slot'] ?? "",
    type: data,
  );
}
