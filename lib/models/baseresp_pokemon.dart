part of 'models.dart';

class BaserespPokemon {
  final int count;
  final List<Pokemon> listPokemon;

  BaserespPokemon({required this.count, required this.listPokemon});

  factory BaserespPokemon.fromJson(Map<String, dynamic> json) =>
      _baserespCompetitionsFromJson(json);
}

BaserespPokemon _baserespCompetitionsFromJson(Map<String, dynamic> json) {
  if (json['results'] != null) {
    var resultObjsJson = json['results'] as List;
    var list = resultObjsJson
        .map(
            (tagJson) => Pokemon.fromJson(tagJson))
        .toList();
    return BaserespPokemon(count: json['count'] ?? 0,listPokemon: list);
  } else {
    return BaserespPokemon(count: json['count'] ?? 0, listPokemon: dummylist);
  }
}