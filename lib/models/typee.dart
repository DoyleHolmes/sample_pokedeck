part of 'models.dart';

class Typee {
  final String name;
  final String url;

  Typee({required this.name, required this.url});

  factory Typee.fromJson(Map<String, dynamic> json) => _typeFromJson(json);
}

Typee _typeFromJson(Map<String, dynamic> json) {
  // var dataJson = json['area'];
  // Area data = Area.fromJson(dataJson);

  return Typee(
    name: json['name'] ?? "",
    url: json['url'] ?? "",
  );
}
