part of 'models.dart';

class DetailPokemonDB {
  String? idStr;
  String? name;

  DetailPokemonDB({this.idStr, this.name});

  DetailPokemonDB.fromMap(Map<dynamic, dynamic> map) {
    idStr = map[columnIdStr] as String?;
    name = map[columnName] as String?;
  }
}
