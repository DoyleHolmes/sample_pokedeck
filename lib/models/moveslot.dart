part of 'models.dart';

class Moveslot {
  final Move move;

  Moveslot({required this.move});

  factory Moveslot.fromJson(Map<String, dynamic> json) =>
      _moveslotFromJson(json);
}

Moveslot _moveslotFromJson(Map<String, dynamic> json) {
  var dataJson = json['move'];
  Move data = Move.fromJson(dataJson);

  return Moveslot(
    move: data,
  );
}
