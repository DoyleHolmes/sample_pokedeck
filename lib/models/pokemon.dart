part of 'models.dart';

class Pokemon {
  final String name;
  final String url;

  Pokemon({required this.name, required this.url});

  factory Pokemon.fromJson(Map<String, dynamic> json) => _pokemonFromJson(json);
}

Pokemon _pokemonFromJson(Map<String, dynamic> json) {
  // var dataJson = json['area'];
  // Area data = Area.fromJson(dataJson);

  return Pokemon(
    name: json['name'] ?? "",
    url: json['url'] ?? "",
  );
}
