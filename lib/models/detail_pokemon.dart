part of 'models.dart';

class DetailPokemon {
  final List<Moveslot> listMove;
  final int id;
  final String name;
  final List<Typeslot> listTypeslot;

  DetailPokemon(
      {required this.listMove,
      required this.id,
      required this.name,
      required this.listTypeslot});

  factory DetailPokemon.fromJson(Map<String, dynamic> json) =>
      _detailPokemonFromJson(json);
}

DetailPokemon _detailPokemonFromJson(Map<String, dynamic> json) {
  var movesObjsJson = json['moves'] as List;
  var listmo = movesObjsJson.map((tagJson) => Moveslot.fromJson(tagJson)).toList();
  var typeslotObjsJson = json['types'] as List;
  var listty = typeslotObjsJson.map((ii) => Typeslot.fromJson(ii)).toList();

  return DetailPokemon(
    name: json['name'] ?? "",
    id: json['id'] ?? 0,
    listMove: listmo,
    listTypeslot: listty,
  );
}
