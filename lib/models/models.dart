import 'package:pokomon_sample/shared/shared.dart';


part 'pokemon.dart';
part 'baseresp_pokemon.dart';
part 'detail_pokemon.dart';
part 'move.dart';
part 'typeslot.dart';
part 'typee.dart';
part 'moveslot.dart';
part 'detail_pokemon_db.dart';