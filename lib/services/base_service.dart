part of 'services.dart';

class BaseServices {
  static Future<dynamic> postRequest(String url, Map jsonMap) async {
    HttpClient httpClient = HttpClient();
    httpClient.connectionTimeout = const Duration(seconds: 30);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('X-Auth-Token', tokenApi);
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    Map<String, dynamic> data =
        json.decode(await response.transform(utf8.decoder).join());
    return data;
  }

  static Future<dynamic> getRequest(String url) async {
    HttpClient httpClient = HttpClient();
    httpClient.connectionTimeout = const Duration(seconds: 30);
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('X-Auth-Token', tokenApi);
    HttpClientResponse response = await request.close();
    if (response.statusCode == 200) {
      return json.decode(await response.transform(utf8.decoder).join());
    } else {
      return response.statusCode;
    }
  }
}
