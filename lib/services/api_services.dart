part of 'services.dart';


class ApiService {
  static Future<dynamic> getPokemon() async {
    dynamic resp = await BaseServices.getRequest(
        baseUrl + getDataPokemon);
    try {
      return BaserespPokemon.fromJson(resp);
    } catch (e) {
      debugPrint(e.toString());
      return resp;
    }
  }

  static Future<dynamic> getDetailPokemon(String number) async {
    dynamic resp = await BaseServices.getRequest(
        baseUrl + getDataPokemon + number);
    try {
      return DetailPokemon.fromJson(resp);
    } catch (e) {
      debugPrint(e.toString());
      return resp;
    }
  }
}