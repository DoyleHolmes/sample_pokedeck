
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:pokomon_sample/models/models.dart';
import 'package:pokomon_sample/shared/shared.dart';


part 'base_service.dart';
part 'api_services.dart';