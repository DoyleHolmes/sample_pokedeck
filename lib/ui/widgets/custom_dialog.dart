part of 'widgets.dart';

class CustomDialogBox extends StatefulWidget {
  final CustomDialogCallback customDialogCallback;
  final String idpokemon;

  const CustomDialogBox(
      {Key? key, required this.customDialogCallback, required this.idpokemon})
      : super(key: key);

  @override
  _CustomDialogBoxState createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(defaultDoublePadding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    final myController = TextEditingController();
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Stack(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(
                left: defaultDoublePadding,
                top: defaultAvatarRadius + 90,
                right: defaultDoublePadding,
                bottom: defaultDoublePadding),
            margin: const EdgeInsets.only(top: defaultAvatarRadius),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(defaultDoublePadding),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  controller: myController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter name',
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Container(
                        margin: const EdgeInsets.only(top: 20),
                        child: ElevatedButton(
                          onPressed: () {
                            if (myController.text.isNotEmpty) {
                              widget.customDialogCallback.onPositive(
                                  myController.text, widget.idpokemon);
                            } else {
                              Flushbar(
                                duration: const Duration(seconds: 1),
                                flushbarPosition: FlushbarPosition.TOP,
                                backgroundColor: Colors.red,
                                message: "You must enter pokemon name",
                              ).show(context);
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.all(0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(80.0)),
                          ),
                          child: Ink(
                            decoration: BoxDecoration(
                                color: accentColor2,
                                borderRadius: BorderRadius.circular(30.0)),
                            child: Container(
                              width: 130,
                              height: 40,
                              alignment: Alignment.center,
                              child: const Text(
                                'SAVE',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const Expanded(flex: 1, child: SizedBox()),
                    Expanded(
                      flex: 5,
                      child: Container(
                        margin: const EdgeInsets.only(top: 20),
                        child: ElevatedButton(
                          onPressed: () {
                            widget.customDialogCallback.onNegative();
                          },
                          style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.all(0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(80.0)),
                          ),
                          child: Ink(
                            decoration: BoxDecoration(
                                color: accentColor2,
                                borderRadius: BorderRadius.circular(30.0)),
                            child: Container(
                              width: 130,
                              height: 40,
                              alignment: Alignment.center,
                              child: const Text(
                                'CANCEL',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
