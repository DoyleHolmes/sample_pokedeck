part of 'widgets.dart';

abstract class CustomDialogCallback {
  void onPositive(String name, String idpokemon);
  void onNegative();
}