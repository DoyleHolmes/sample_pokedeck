part of 'widgets.dart';

class PokemonContainer extends StatefulWidget {
  final String codeImg;
  final String label;
  final Function(String numberPokemon, String namePokemon) onTap;

  const PokemonContainer({
    Key? key,
    required this.codeImg,
    required this.label,
    required this.onTap,
  }) : super(key: key);

  @override
  _PokemonContainerState createState() => _PokemonContainerState();
}

class _PokemonContainerState extends State<PokemonContainer> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        widget.onTap(widget.codeImg, widget.label);
      },
      child: Card(
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Container(
          decoration:
              BoxDecoration(border: Border.all(color: Colors.grey, width: 1.3)),
          child: Column(
            children: [
              CachedNetworkImage(
                imageUrl: urlImgPokemon + widget.codeImg + '.png',
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    CircularProgressIndicator(value: downloadProgress.progress),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
              Text(widget.label),
              const SizedBox(
                height: 3,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
