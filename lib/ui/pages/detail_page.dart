part of 'pages.dart';

class DetailPage extends StatefulWidget {
  final String numberPokemon;
  final String namePokemon;
  final String type;

  DetailPage(this.numberPokemon, this.namePokemon, this.type);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage>
    implements CustomDialogCallback {
  bool isNetworkAvail = false, isShowLoading = false;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();

    isNetworkAvail = await Innet.isAvailable();
    if (!isNetworkAvail) {
      Flushbar(
        duration: const Duration(milliseconds: 1500),
        flushbarPosition: FlushbarPosition.TOP,
        backgroundColor: const Color(0xFFFF5C83),
        message: "No Internet Connection",
      ).show(context);
    }
  }

  Future<bool> redirectTo() async {
    if (widget.type == 'internet') {
      Navigator.of(context).push(
        MaterialPageRoute(builder: (_) => HomePage()),
      );
    } else {
      Navigator.of(context).push(
        MaterialPageRoute(builder: (_) => MyHomePage()),
      );
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: WillPopScope(
        onWillPop: () async {
          return redirectTo();
        },
        child: Scaffold(
          backgroundColor: const Color(0xffffffff),
          floatingActionButton: widget.type == 'internet'
              ? FloatingActionButton(
                  heroTag: null,
                  onPressed: () {
                    var rng = Random();
                    debugPrint(rng.nextBool().toString());
                    if (rng.nextBool()) {
                      showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (BuildContext context) {
                            return CustomDialogBox(
                              idpokemon: widget.numberPokemon,
                              customDialogCallback: this,
                            );
                          });
                    } else {
                      Flushbar(
                        duration: const Duration(seconds: 1),
                        flushbarPosition: FlushbarPosition.TOP,
                        backgroundColor: Colors.red,
                        message: "Failed to catch pokemon",
                      ).show(context);
                    }
                  },
                  child: const Icon(Icons.add_circle),
                  backgroundColor: Colors.green,
                )
              : FloatingActionButton(
                  heroTag: null,
                  onPressed: () {
                    deleteData(widget.namePokemon);
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) => HomePage()),
                    );
                  },
                  child: const Icon(Icons.wifi_protected_setup),
                  backgroundColor: Colors.green,
                ),
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: FutureBuilder(
                future: ApiService.getDetailPokemon(widget.numberPokemon),
                builder: (context, snapshot) {
                  DetailPokemon? detailPokemon;
                  if (snapshot.hasData) {
                    detailPokemon = snapshot.data as DetailPokemon;
                    var movesss = '';
                    var typeeeee = '';
                    for (var i in detailPokemon.listMove) {
                      movesss = movesss + i.move.name + ',';
                    }
                    for (var g in detailPokemon.listTypeslot) {
                      typeeeee = typeeeee + g.type.name + ',';
                    }

                    return SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: CustomScrollView(
                          slivers: [
                            SliverFillRemaining(
                              hasScrollBody: false,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  CachedNetworkImage(
                                    imageUrl: urlImgPokemon +
                                        widget.numberPokemon +
                                        '.png',
                                    progressIndicatorBuilder: (context, url,
                                            downloadProgress) =>
                                        CircularProgressIndicator(
                                            value: downloadProgress.progress),
                                    errorWidget: (context, url, error) =>
                                        const Icon(Icons.error),
                                  ),
                                  Text(widget.namePokemon),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(' = MOVES = '),
                                  Expanded(
                                    flex: 1,
                                    child: Text(movesss),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(' = TYPES = '),
                                  Expanded(
                                    flex: 1,
                                    child: Text(typeeeee),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ));
                  } else {
                    return const Text('');
                  }
                }),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void onPositive(String name, String idpokemon) {
    Navigator.of(context).pop();
    debugPrint('??????????????????????');
    debugPrint(name);
    insertData(idpokemon, name);

    Flushbar(
      duration: const Duration(milliseconds: 1500),
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Colors.green,
      message: "Saved",
    ).show(context);
  }

  @override
  void onNegative() {
    Navigator.of(context).pop();
  }
}
