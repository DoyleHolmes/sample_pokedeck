part of 'pages.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  bool isNetworkAvail = false, isShowLoading = false;
  AppLifecycleState _notification = AppLifecycleState.resumed;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _notification = state;
      debugPrint('>>>>>>>>>>>>>>>>>>>>>>> $_notification');
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await initDatabase();
    isNetworkAvail = await Innet.isAvailable();
    if (!isNetworkAvail) {
      Flushbar(
        duration: const Duration(milliseconds: 1500),
        flushbarPosition: FlushbarPosition.TOP,
        backgroundColor: const Color(0xFFFF5C83),
        message: "No Internet Connection",
      ).show(context);
    }
  }

  Future<bool> redirectTo() async {

    context.read<PageBloc>().add(
        GoToHomePage());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return redirectTo();
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: const Color(0xffffffff),
          body: LoadingOverlay(
            isLoading: isShowLoading,
            opacity: 0.2,
            progressIndicator: const CircularProgressIndicator(),
            color: Colors.blueGrey,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: FutureBuilder(
                  future: getAllData(),
                  builder: (context, snapshot) {
                    List<DetailPokemonDB>? listDetailPokemonDB;
                    if (snapshot.hasData) {
                      listDetailPokemonDB =
                          snapshot.data as List<DetailPokemonDB>;
                      debugPrint('************************');
                      debugPrint('${listDetailPokemonDB.length}');
                      return GridView.count(
                        childAspectRatio: 1.0,
                        crossAxisCount: 2,
                        children: <Widget>[
                          for (var i in listDetailPokemonDB)
                            PokemonContainer(
                              onTap:
                                  (String numberPokemon, String namePokemon) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetailPage(
                                          numberPokemon, namePokemon, 'local')),
                                );
                                // context.read<PageBloc>().add(
                                //     GoToDetailPage(numberPokemon, namePokemon, 'local'));
                              },
                              codeImg: i.idStr.toString(),
                              label: i.name.toString(),
                            )
                        ],
                      );
                    } else {
                      debugPrint(
                          '************************????? ${snapshot.data}');
                      return const Text('');
                    }
                  }),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }
}
