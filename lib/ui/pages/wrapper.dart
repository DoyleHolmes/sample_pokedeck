part of 'pages.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<PageBloc>(context).add(GoToSplashPage());

    return BlocBuilder<PageBloc, PageState>(builder: (_, pageState) {
      if (pageState is OnHomePage) {
        return HomePage();
      } else if (pageState is OnDetailPage) {
        return DetailPage(
            pageState.numberPokemon, pageState.namePokemon, pageState.type);
      } else if (pageState is OnSplashPage) {
        return SplashScreenPage();
      } else if (pageState is OnMyHomePage) {
        return MyHomePage();
      } else {
        return HomePage();
      }
    });
  }
}
