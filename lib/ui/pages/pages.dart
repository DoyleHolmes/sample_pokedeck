

import 'dart:math';

import 'package:another_flushbar/flushbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:pokomon_sample/blocs/blocs.dart';
import 'package:pokomon_sample/helper/helper.dart';
import 'package:pokomon_sample/models/models.dart';
import 'package:pokomon_sample/services/services.dart';
import 'package:pokomon_sample/shared/shared.dart';
import 'package:pokomon_sample/ui/widgets/widgets.dart';

part 'wrapper.dart';
part 'home_page.dart';
part 'detail_page.dart';
part 'splash_screen.dart';
part 'myhome_page.dart';