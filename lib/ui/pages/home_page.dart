part of 'pages.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isNetworkAvail = false, isShowLoading = false;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await initDatabase();

    isNetworkAvail = await Innet.isAvailable();
    if (!isNetworkAvail) {
      Flushbar(
        duration: const Duration(milliseconds: 1500),
        flushbarPosition: FlushbarPosition.TOP,
        backgroundColor: const Color(0xFFFF5C83),
        message: "No Internet Connection",
      ).show(context);
    }
  }

  Future<bool> redirectTo() async {
    // Navigator.of(context)
    //     .push(
    //   MaterialPageRoute(builder: (_) => MyHomePage()),
    // );
    // Navigator.pop(context,true);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return redirectTo();
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: const Color(0xffffffff),
          floatingActionButton: FloatingActionButton(
            heroTag: null,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyHomePage()),
              );
            },
            child: const Icon(Icons.save),
            backgroundColor: Colors.green,
          ),
          body: LoadingOverlay(
            isLoading: isShowLoading,
            opacity: 0.2,
            progressIndicator: const CircularProgressIndicator(),
            color: Colors.blueGrey,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: FutureBuilder(
                  future: ApiService.getPokemon(),
                  builder: (_, snapshot) {
                    BaserespPokemon? baserespPokemon;
                    if (snapshot.hasData) {
                      baserespPokemon = snapshot.data as BaserespPokemon;
                      debugPrint('${baserespPokemon.listPokemon.length}');
                      return GridView.count(
                        childAspectRatio: 1.0,
                        crossAxisCount: 2,
                        children: <Widget>[
                          for (var i in baserespPokemon.listPokemon)
                            PokemonContainer(
                              onTap:
                                  (String numberPokemon, String namePokemon) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetailPage(
                                          numberPokemon, namePokemon, 'internet')),
                                );
                              },
                              codeImg:
                                  i.url.split('pokemon')[1].replaceAll('/', ''),
                              label: i.name,
                            )
                        ],
                      );
                    } else {
                      return const Text('');
                    }
                  }),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
