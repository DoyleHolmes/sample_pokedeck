part of 'helper.dart';

late Database database;

Future<dynamic> initDatabase() async {
  var databasesPath = await getDatabasesPath();
  String path = join(databasesPath, 'myDB.db');
  try {
    /// open the database
    database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      /// When creating the db, create the table
      await db.execute(
          'CREATE TABLE mypokemon (id INTEGER PRIMARY KEY, idStr TEXT, name TEXT)');
    });
    return database;
  } catch (e) {
    debugPrint(e.toString());
    return false;
  }
}

insertData(String idStr, String name) async {
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'INSERT INTO mypokemon(idStr, name) VALUES("$idStr", "$name")');
    debugPrint('inserted1: $id1');
  });
}

Future<List<DetailPokemonDB>> getAllData() async {
  List<Map> list = await database.rawQuery('SELECT * FROM mypokemon');
  debugPrint(list.toString());
  return list.map((e) => DetailPokemonDB.fromMap(e)).toList();
}

deleteData(String name) async {
  // int count =
      await database.rawDelete('DELETE FROM mypokemon WHERE name = ?', [name]);
  // debugPrint('delete1: $count');
  // assert(count == 1);
}