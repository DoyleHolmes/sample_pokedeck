import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'page/page_state.dart';
part 'page/page_event.dart';
part 'page/page_bloc.dart';

part 'theme/theme_state.dart';
part 'theme/theme_event.dart';
part 'theme/theme_bloc.dart';