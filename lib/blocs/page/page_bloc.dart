part of '../blocs.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  PageBloc() : super(OnInitialPage());

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if (event is GoToHomePage) {
      yield OnHomePage();
    } else if (event is GoToDetailPage) {
      yield OnDetailPage(event.numberPokemon, event.namePokemon, event.type);
    } else if (event is GoToSplashPage) {
      yield OnSplashPage();
    } else if (event is GoToMyHomePage) {
      yield OnMyHomePage();
    }
  }
}
