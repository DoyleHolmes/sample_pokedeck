part of '../blocs.dart';

abstract class PageState {
  const PageState();
}

class OnInitialPage extends PageState {}

class OnHomePage extends PageState {}

class OnMyHomePage extends PageState {}

class OnSplashPage extends PageState {}

class OnDetailPage extends PageState {
  final String numberPokemon;
  final String namePokemon;
  final String type;

  OnDetailPage(this.numberPokemon, this.namePokemon, this.type);
}
