part of '../blocs.dart';

abstract class PageEvent {
  const PageEvent();
}

class GoToHomePage extends PageEvent {}

class GoToMyHomePage extends PageEvent {}

class GoToDetailPage extends PageEvent {
  final String numberPokemon;
  final String namePokemon;
  final String type;

  GoToDetailPage(this.numberPokemon, this.namePokemon, this.type);
}

class GoToSplashPage extends PageEvent {}
